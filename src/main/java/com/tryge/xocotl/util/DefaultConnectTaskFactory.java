package com.tryge.xocotl.util;

/**
 * @author michael.zehender@me.com
 */
public class DefaultConnectTaskFactory implements ConnectTaskFactory {
	@Override
	public ConnectTask newInstance(ConnectCallback callback, ConnectConfig config) {
		return new DefaultConnectTask(callback, config);
	}
}

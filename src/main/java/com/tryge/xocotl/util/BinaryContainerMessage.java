package com.tryge.xocotl.util;

import com.tryge.xocotl.io.Message;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BinaryContainerMessage implements Message {
	private final byte[] content;

	public BinaryContainerMessage(byte[] content) {
		Preconditions.notNull(content, "content must not be null");

		this.content = content;
	}

	public byte[] getContent() {
		return content;
	}

	@Override
	public String getId() {
		return null;
	}

	@Override
	public String isResponseTo() {
		return null;
	}

	@Override
	public boolean isResponse() {
		return false;
	}

	@Override
	public void writeTo(OutputStream outputStream) throws IOException {
		DataOutputStream out = new DataOutputStream(outputStream);
		out.writeInt(content.length);
		out.write(content);
		out.flush();
	}
}

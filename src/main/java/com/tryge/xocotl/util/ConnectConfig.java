package com.tryge.xocotl.util;

import com.tryge.xocotl.io.MessageDecoder;
import com.tryge.xocotl.io.MessageListener;
import com.tryge.xocotl.io.SocketChannelFactory;
import com.tryge.xocotl.io.SocketChannels;

import java.net.SocketAddress;

/**
 * @author michael.zehender@me.com
 */
public class ConnectConfig {
	private SocketAddress address;
	private SocketChannelFactory factory;
	private MessageDecoder decoder;
	private MessageListener listener;
	private int connectTimeout = 500;

	/**
	 * Default Constructor.
	 */
	public ConnectConfig() {
	}

	/**
	 * Copy Constructor.
	 *
	 * @param config to copy
	 */
	public ConnectConfig(ConnectConfig config) {
		address = config.getAddress();
		factory = config.getFactory();
		decoder = config.getDecoder();
		listener = config.getListener();
		connectTimeout = config.getConnectTimeout();
	}

	public SocketAddress getAddress() {
		return address;
	}

	public void setAddress(SocketAddress address) {
		this.address = address;
	}

	public SocketChannelFactory getFactory() {
		if (factory == null) {
			return SocketChannels.getFactory();
		}
		return factory;
	}

	public void setFactory(SocketChannelFactory factory) {
		this.factory = factory;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public MessageDecoder getDecoder() {
		return decoder;
	}

	public void setDecoder(MessageDecoder decoder) {
		this.decoder = decoder;
	}

	public MessageListener getListener() {
		return listener;
	}

	public void setListener(MessageListener listener) {
		this.listener = listener;
	}

	public boolean isValid() {
		return address != null && decoder != null && listener != null && connectTimeout > 0;
	}
}

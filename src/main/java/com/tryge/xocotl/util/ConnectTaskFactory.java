package com.tryge.xocotl.util;

/**
 * @author michael.zehender@me.com
 */
public interface ConnectTaskFactory {
	/**
	 * Create a new instance of a ConnectTask with the specified
	 * information.
	 *
	 * @param callback for notifications
	 * @param config with information where to connect to and how to handle
	 *               messages
	 * @return the connect task
	 */
	ConnectTask newInstance(ConnectCallback callback, ConnectConfig config);
}

package com.tryge.xocotl.util;

/**
 * @author michael.zehender@me.com
 */
public interface ConnectTask extends Runnable {
	/**
	 * @return whether a connection is established
	 */
	boolean isConnected();
}

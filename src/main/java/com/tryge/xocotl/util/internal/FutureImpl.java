package com.tryge.xocotl.util.internal;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author michael.zehender@me.com
 */
public class FutureImpl<T> implements Future<T> {
	private boolean cancelled;
	private boolean running;
	private boolean done;
	private T value;
	private ExecutionException exception;

	private final Runnable doneAction;

	public FutureImpl(Runnable doneAction) {
		this.doneAction = doneAction;
	}

	@Override
	public synchronized boolean cancel(boolean mayInterruptIfRunning) {
		if (!isDone() && (!running || mayInterruptIfRunning)) {
			cancelled = true;
			this.notifyAll();
			doneAction.run();
		}
		return isCancelled();
	}

	@Override
	public synchronized boolean isCancelled() {
		return cancelled;
	}

	@Override
	public synchronized boolean isDone() {
		return done || isCancelled();
	}

	@Override
	public synchronized T get() throws InterruptedException, ExecutionException {
		while (!isDone()) {
			this.wait();
		}
		if (exception != null) {
			throw exception;
		}
		return value;
	}

	@Override
	public synchronized T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		long now = System.currentTimeMillis();
		long deadline = now + unit.toMillis(timeout);

		while (((now = System.currentTimeMillis()) < deadline) && !isDone()) {
			this.wait(deadline - now);
		}
		if (!isDone()) {
			throw new TimeoutException();
		}
		if (exception != null) {
			throw exception;
		}

		return value;
	}

	public synchronized void running() {
		this.running = true;
	}

	public synchronized void succeeded(T value) {
		if (!isDone()) {
			this.value = value;
			this.done = true;
			this.notifyAll();
			doneAction.run();
		}
	}

	public synchronized void failed(Throwable th) {
		if (!isDone()) {
			this.exception = new ExecutionException(th);
			this.done = true;
			this.notifyAll();
			doneAction.run();
		}
	}
}

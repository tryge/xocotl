package com.tryge.xocotl.util.internal;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * @author michael.zehender@me.com
 */
public class IoHelper {
	public static void closeSilently(Closeable closeable) {
		try {
			closeable.close();
		} catch (IOException ignored) {
		}
	}

	public static void closeSilently(ServerSocket socket) {
		try {
			socket.close();
		} catch (IOException ignored) {
		}
	}
}

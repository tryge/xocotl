package com.tryge.xocotl.util.internal;

/**
 * @author michael.zehender@me.com
 */
public class Nop implements Runnable {
	@Override
	public void run() {
		// nop
	}
}

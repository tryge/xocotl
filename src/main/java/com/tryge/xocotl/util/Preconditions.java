package com.tryge.xocotl.util;

/**
 * @author michael.zehender@me.com
 */
public class Preconditions {
	private Preconditions() {
		// hide utility class constructor
	}

	public static <T> void notNull(T object, String message) {
		if (object == null) {
			throw new NullPointerException(message);
		}
	}

	public static void checkArgument(boolean condition, String message) {
		if (!condition) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void checkState(boolean condition, String message) {
		if (!condition) {
			throw new IllegalStateException(message);
		}
	}

	public static void checkPositive(int receiveBufferSize, String message) {
		if (receiveBufferSize <= 0) {
			throw new IllegalArgumentException(message);
		}
	}
}

package com.tryge.xocotl.util;

import com.tryge.xocotl.io.*;
import com.tryge.xocotl.util.internal.IoHelper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/**
 * @author michael.zehender@me.com
 */
public class BoundedServer {
	private final ChannelFactory channelFactory;
	private final ChannelListener channelListener;
	private final MessageDecoder messageDecoder;
	private final MessageListener messageListener;
	private final Semaphore semaphore;

	private final Set<DuplexChannel> channels = Collections.newSetFromMap(new ConcurrentHashMap<DuplexChannel, Boolean>());

	private volatile Thread runner;
	private volatile ServerSocket serverSocket;

	private BoundedServer(ChannelFactory channelFactory, ChannelListener channelListener,
	                      MessageDecoder messageDecoder, MessageListener messageListener,
	                      int maximumConnections) {
		this.channelFactory = channelFactory;
		this.channelListener = new InnerChannelListener(channelListener);
		this.messageDecoder= messageDecoder;
		this.messageListener= messageListener;
		this.semaphore = new Semaphore(maximumConnections);
	}

	public synchronized void start(ServerSocket listeningSocket) {
		if (isServerRunning()) {
			throw new IllegalStateException("server has already been started");
		}

		serverSocket = listeningSocket;

		runner = new Thread(new Runnable() {
			@Override
			public void run() {
				BoundedServer.this.run();
			}
		});
		runner.start();
	}

	private void run() {
		try {
			while (!Thread.currentThread().isInterrupted()) {
				semaphore.acquire();

				Socket client = serverSocket.accept();
				DuplexChannel channel = channelFactory.newSocketChannel(client, messageDecoder);

				channels.add(channel);

				channel.setChannelListener(channelListener);
				channel.setMessageListener(messageListener);
				channel.open();
			}
		} catch (IOException ignored) {
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} finally {
			closeConnectedChannels();
		}
	}

	private void closeConnectedChannels() {
		for (DuplexChannel channel : channels) {
			IoHelper.closeSilently(channel);
		}
	}

	public synchronized void stop() throws InterruptedException {
		if (isServerRunning()) {
			runner.interrupt();
			IoHelper.closeSilently(serverSocket);
			runner.join(1000);

			serverSocket = null;
			runner = null;
		}
	}

	public boolean isServerRunning() {
		return runner != null && runner.isAlive();
	}

	private class InnerChannelListener implements ChannelListener {
		private final ChannelListener delegate;

		public InnerChannelListener(ChannelListener delegate) {
			this.delegate = delegate;
		}

		@Override
		public void onOpen(DuplexChannel channel) {
			if (delegate != null) {
				delegate.onOpen(channel);
			}
		}

		@Override
		public void onClose(DuplexChannel channel) {
			try {
				if (delegate != null) {
					delegate.onClose(channel);
				}
			} finally {
				channels.remove(channel);
				semaphore.release();
			}
		}
	}

	public static class Builder {
		private ChannelFactory channelFactory = SocketChannels.getFactory();
		private ChannelListener channelListener;
		private MessageDecoder messageDecoder;
		private MessageListener messageListener;
		private int maximumConnections = 1;

		public Builder setChannelFactory(ChannelFactory channelFactory) {
			this.channelFactory = channelFactory;
			return this;
		}

		public Builder setChannelListener(ChannelListener channelListener) {
			this.channelListener = channelListener;
			return this;
		}

		public Builder setMessageDecoder(MessageDecoder messageDecoder) {
			this.messageDecoder = messageDecoder;
			return this;
		}

		public Builder setMessageListener(MessageListener messageListener) {
			this.messageListener = messageListener;
			return this;
		}

		public Builder setMaximumConnections(int maximumConnections) {
			this.maximumConnections = maximumConnections;
			return this;
		}

		public BoundedServer build() {
			Preconditions.notNull(channelFactory, "need channel factory to create channel when accepting a connection");
			Preconditions.notNull(messageDecoder, "need message decoder to decode incoming messages");
			Preconditions.notNull(messageListener, "need message listener to notify about received messages");

			return new BoundedServer(
				channelFactory,
				channelListener,
				messageDecoder,
				messageListener,
				maximumConnections
			);
		}
	}
}

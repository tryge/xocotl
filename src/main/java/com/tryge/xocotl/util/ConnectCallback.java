package com.tryge.xocotl.util;

import com.tryge.xocotl.io.DuplexChannel;

/**
 * @author michael.zehender@me.com
 */
public interface ConnectCallback {
	/**
	 * This method is called to verify the connection before calling
	 * onConnect(), if this method returns false, the channel is closed.
	 *
	 * @param channel to check
	 * @return whether the connection is valid
	 */
	boolean checkChannel(DuplexChannel channel);

	/**
	 * Called upon connect.
	 *
	 * @param channel that has connected
	 */
	void onConnect(DuplexChannel channel);

	/**
	 * Called upon disconnect.
	 */
	void onDisconnect();
}

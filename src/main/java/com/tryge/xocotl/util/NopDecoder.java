package com.tryge.xocotl.util;

import com.tryge.xocotl.io.Message;
import com.tryge.xocotl.io.MessageDecoder;

import java.nio.ByteBuffer;

public class NopDecoder implements MessageDecoder {
	@Override
	public Message decode(ByteBuffer buffer) {
		return null;
	}
}

package com.tryge.xocotl.util;

import com.tryge.xocotl.io.ChannelListener;
import com.tryge.xocotl.io.DuplexChannel;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;

/**
 * This class is meant to be used with the ConnectManager that uses a
 * ScheduledExecutorService to repeatedly try to connect to the specified
 * server.
 *
 * @author michael.zehender@me.com
 */
public class DefaultConnectTask implements ConnectTask, ChannelListener {
	private final ConnectCallback callback;
	private final ConnectConfig config;

	private volatile boolean connected = false;
	private volatile DuplexChannel channel;

	public DefaultConnectTask(ConnectCallback callback, ConnectConfig config) {
		Preconditions.notNull(callback, "callback mustn't be null");
		Preconditions.notNull(config, "config mustn't be null");
		Preconditions.checkArgument(config.isValid(), "config is not valid");

		this.callback = callback;
		this.config = new ConnectConfig(config);
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket();
			socket.connect(config.getAddress(), config.getConnectTimeout());

			channel = config.getFactory().newSocketChannel(socket, config.getDecoder());
			channel.setChannelListener(this);
			channel.setMessageListener(config.getListener());
			channel.open();

			if (callback.checkChannel(channel)) {
				connected = true;
				callback.onConnect(channel);
			} else {
				closeSilently(channel);
				channel = null;
			}
		} catch(IOException e) {
			closeSilently(channel);
			connected = false;
			channel = null;
		}
	}

	@Override
	public void onOpen(DuplexChannel channel) {
	}

	@Override
	public void onClose(DuplexChannel ignored) {
		if (connected) {
			callback.onDisconnect();
			closeSilently(channel);
			connected = false;
			channel = null;
		}
	}

	private void closeSilently(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ioe) {
			// ignore
		}
	}
}

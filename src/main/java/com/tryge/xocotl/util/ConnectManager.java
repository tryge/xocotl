package com.tryge.xocotl.util;

import com.tryge.xocotl.io.DuplexChannel;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author michael.zehender@me.com
 */
public class ConnectManager {
	private final ScheduledExecutorService scheduler;
	private final ConnectTask connectTask;

	private long retryInterval;
	private ScheduledFuture<?> future;

	private boolean started = false;

	/**
	 * Construct the ConnectManager with the DefaultConnectTaskFactory and
	 * the specified parameters.
	 *
	 * @param scheduler to schedule tasks
	 * @param callback for connect/disconnect notifications
	 * @param config where to connect to and how to handle messages
	 */
	public ConnectManager(ScheduledExecutorService scheduler, ConnectCallback callback, ConnectConfig config) {
		this(scheduler, callback, config, new DefaultConnectTaskFactory());
	}

	/**
	 * Construct the ConnectManager with the specified parameters.
	 *
	 * @param scheduler to schedule tasks
	 * @param callback for connect/disconnect notifications
	 * @param config where to connect to and how to handle messages
	 * @param factory to instantiate connect tasks
	 */
	public ConnectManager(ScheduledExecutorService scheduler, ConnectCallback callback, ConnectConfig config, ConnectTaskFactory factory) {
		Preconditions.notNull(scheduler, "scheduler mustn't be null");
		Preconditions.notNull(callback,  "callback mustn't be null");

		this.scheduler = scheduler;
		this.connectTask = factory.newInstance(new DelegatingConnectCallback(callback), config);
	}

	public boolean isConnected() {
		return connectTask.isConnected();
	}

	public synchronized void start(long retryInterval) {
		this.retryInterval = retryInterval;

		future = scheduler.scheduleAtFixedRate(
			connectTask,
			0L,
			retryInterval,
			TimeUnit.MILLISECONDS
		);

		started = true;
	}

	public synchronized void stop() {
		if (future != null) {
			future.cancel(true);
		}

		started = false;
	}

	private class DelegatingConnectCallback implements ConnectCallback {
		private final ConnectCallback callback;

		private DelegatingConnectCallback(ConnectCallback callback) {
			this.callback = callback;
		}

		@Override
		public boolean checkChannel(DuplexChannel channel) {
			return callback.checkChannel(channel);
		}

		@Override
		public void onConnect(final DuplexChannel channel) {
			synchronized (ConnectManager.this) {
				if (future != null) {
					future.cancel(false);
				}
			}

			scheduler.submit(new Runnable() {
				@Override
				public void run() {
					callback.onConnect(channel);
				}
			});
		}

		@Override
		public void onDisconnect() {
			scheduler.submit(new Runnable() {
				@Override
				public void run() {
					callback.onDisconnect();
				}
			});

			synchronized (ConnectManager.this) {
				if (started) {
					future = scheduler.scheduleAtFixedRate(
						connectTask,
						retryInterval,
						retryInterval,
						TimeUnit.MILLISECONDS
					);
				}
			}
		}
	}
}

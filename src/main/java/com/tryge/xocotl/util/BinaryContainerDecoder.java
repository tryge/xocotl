package com.tryge.xocotl.util;

import com.tryge.xocotl.io.Message;
import com.tryge.xocotl.io.MessageDecoder;

import java.nio.ByteBuffer;

public class BinaryContainerDecoder implements MessageDecoder {
	@Override
	public Message decode(ByteBuffer buffer) {
		if (buffer.remaining() < 4) {
			return null;
		}

		int length = buffer.getInt();
		if (buffer.remaining() < length) {
			return null;
		}

		byte[] bytes = new byte[length];
		buffer.get(bytes);

		return new BinaryContainerMessage(bytes);
	}
}

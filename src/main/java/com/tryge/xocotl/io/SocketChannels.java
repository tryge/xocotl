package com.tryge.xocotl.io;

/**
 * @author michael.zehender@me.com
 */
public class SocketChannels {
	private static final SocketChannelFactory factory = new SocketChannelFactory();

	public static SocketChannelFactory getFactory() {
		return factory;
	}
}

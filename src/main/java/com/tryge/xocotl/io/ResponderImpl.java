package com.tryge.xocotl.io;

import com.tryge.xocotl.util.Preconditions;

import java.io.EOFException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author michael.zehender@me.com
 */
class ResponderImpl implements Responder {
	private final ConcurrentHashMap<Pair, ResponderMessage> messages
		= new ConcurrentHashMap<Pair, ResponderMessage>();

	/**
	 * Method to cleanup messages older than the specified deadline. It is
	 * the responsibility of the owning component to call this method.
	 * However, generally this shouldn't be necessary as the storage is
	 * cleaned after a future succeeded, failed or is cancelled.
	 *
	 * @param deadline older messages are removed
	 */
	public void cleanup(long deadline) {
		Iterator<Map.Entry<Pair, ResponderMessage>> iterator = messages.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<Pair, ResponderMessage> entry = iterator.next();

			if (entry.getKey().timestamp < deadline) {
				iterator.remove();
			}
		}
	}

	int waitingMessages() {
		return messages.size();
	}

	/**
	 * @see Responder#destroyed(Object)
	 */
	@Override
	public void destroyed(Object context) {
		Iterator<Map.Entry<Pair, ResponderMessage>> iterator = messages.entrySet().iterator();
		// TODO measure performance impact if responders are shared (they aren't atm)
		while (iterator.hasNext()) {
			Map.Entry<Pair, ResponderMessage> entry = iterator.next();

			if (entry.getKey().context.equals(context)) {
				entry.getValue().future().failed(new EOFException());
				iterator.remove();
			}
		}
	}

	@Override
	public ResponderMessage register(Object context, Message message) {
		Preconditions.notNull(context, "context mustn't be null");
		Preconditions.notNull(message, "message mustn't be null");
		Preconditions.notNull(message.getId(), "message id mustn't be null");

		final Pair pair = new Pair(context, message.getId());

		ResponderMessage responderMessage = new ResponderMessage(
			message,
			new Runnable() {
				@Override
				public void run() {
					messages.remove(pair);
				}
			}
		);

		messages.put(pair, responderMessage);

		return responderMessage;
	}

	@Override
	public void responseReceived(Object context, Message response) {
		Preconditions.notNull(context, "context mustn't be null");
		Preconditions.notNull(response, "response mustn't be null");
		Preconditions.notNull(response.isResponseTo(), "responseTo id mustn't be null");

		Pair pair = new Pair(context, response.isResponseTo());

		ResponderMessage message = messages.remove(pair);
		if (message != null) {
			message.future().succeeded(response);
		}
	}

	private class Pair {
		private final Object context;
		private final String id;
		private final long timestamp = System.currentTimeMillis();

		private Pair(Object context, String id) {
			this.context = context;
			this.id = id;
		}

		@Override
		public int hashCode() {
			return context.hashCode() * 31 + id.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Pair) {
				Pair other = (Pair)obj;
				return context == other.context && id.equals(other.id);
			}
			return false;
		}
	}
}

package com.tryge.xocotl.io;

import com.tryge.xocotl.util.internal.FutureImpl;
import com.tryge.xocotl.util.internal.Nop;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author michael.zehender@me.com
 */
class SuccessMessage extends AbstractMessageDelegate {
	private final FutureImpl<Void> future = new FutureImpl<Void>(new Nop());

	SuccessMessage(Message message) {
		super(message);
	}

	@Override
	public void writeTo(OutputStream outputStream) throws IOException {
		try {
			synchronized (future) {
				if (!future().isCancelled()) {
					future.running();
					message.writeTo(outputStream);
					future.succeeded(null);
				}
			}
		} catch(Exception e) {
			future.failed(e);
		}
	}

	FutureImpl<Void> future() {
		return future;
	}
}

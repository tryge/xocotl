package com.tryge.xocotl.io;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Future;

/**
 * @author michael.zehender@me.com
 */
public interface DuplexChannel extends Closeable {
	/**
	 * Closes the channel.
	 *
	 * @throws IOException if an exception occurred in the underlying
	 *         implementation
	 */
	void close() throws IOException;

	/**
	 * Opens the channel - details depend on the implementation.
	 *
	 * @throws IOException
	 */
	void open() throws IOException;

	/**
	 * @return whether this channel is closed
	 */
	public boolean isClosed();

	/**
	 * @return whether this channel is open
	 */
	public boolean isOpen();

	/**
	 * Sends the specified message and returns a future to receive the
	 * response. If there won't be a response use sendEx or sendAndForget instead.
	 * Exceptions will be signalled via the future.
	 *
	 * @param msg to send
	 * @return the future to receive a response.
	 */
	Future<Message> send(Message msg);

	/**
	 * Sends the specified message and returns a future to check whether
	 * the message has been sent successfully. This future doesn't wait
	 * for a response, it just waits that the sending of the message
	 * succeeds or fails.
	 *
	 * @param msg to send
	 * @return the future to wait for success or failure
	 */
	Future<Void> sendEx(Message msg);

	/**
	 * Sends the specified message and forgets about it i.e. there won't
	 * be a response. If there is a response it will be delivered to the
	 * dead-letter channel. All exceptions are ignored
	 *
	 * @param msg to send
	 */
	void sendAndForget(Message msg);

	/**
	 * This method sets the channel listener, the channel doesn't maintain
	 * a list of listeners therefore this method overwrites any previously
	 * set channel listener.
	 *
	 * @param listener to notify about channel events
	 */
	void setChannelListener(ChannelListener listener);

	/**
	 * This method sets the message listener, the channel doesn't maintain
	 * a list of listeners therefore this method overwrites any previously
	 * set message listener.
	 *
	 * @param listener to notify about incoming messages
	 */
	void setMessageListener(MessageListener listener);
}

package com.tryge.xocotl.io;

/**
 * @author michael.zehender@me.com
 */
abstract class AbstractMessageDelegate implements Message {
	final Message message;

	protected AbstractMessageDelegate(Message message) {
		this.message = message;
	}

	@Override
	public String getId() {
		return message.getId();
	}

	@Override
	public String isResponseTo() {
		return message.isResponseTo();
	}

	@Override
	public boolean isResponse() {
		return message.isResponse();
	}
}

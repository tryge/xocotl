package com.tryge.xocotl.io;

import java.net.Socket;
import java.util.concurrent.Executors;

/**
 * @author michael.zehender@me.com
 */
public class SocketChannelFactory implements ChannelFactory {
	/**
	 * Creates a socket channel with a read buffer of 65536 bytes. Note that it
	 * is important to cleanup futures i.e. if waiting for a response times
	 * out you have to cancel the future - otherwise the channel will wait
	 * for a response forever.
	 *
	 * @param socket for the channel
	 * @param decoder to decode received messages
	 * @return the socket channel
	 */
	@Override
	public DuplexChannel newSocketChannel(Socket socket, MessageDecoder decoder) {
		return newSocketChannel(socket, decoder, 65536);
	}

	/**
	 * Creates a socket channel with the specified parameters. Note that it
	 * is important to cleanup futures i.e. if waiting for a response times
	 * out you have to cancel the future - otherwise the channel will wait
	 * for a response forever.
	 *
	 * @param socket for the channel
	 * @param decoder to decode received messages
	 * @param readBufferSize maximum size that is read into memory, if more
	 *                       data is received without being able to decode
	 *                       messages (and therefore removing some data
	 *                       from the buffer) a BufferOverflowException
	 *                       will stop the channel
	 * @return the socket channel
	 */
	@Override
	public DuplexChannel newSocketChannel(Socket socket, MessageDecoder decoder, int readBufferSize) {
		ResponderImpl responder = new ResponderImpl();

		return new StreamedDuplexChannel(
			Executors.defaultThreadFactory(),
			responder,
			decoder,
			new SocketStreamSource(socket),
			readBufferSize
		);
	}
}

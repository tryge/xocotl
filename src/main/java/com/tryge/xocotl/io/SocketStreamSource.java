package com.tryge.xocotl.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author michael.zehender@me.com
 */
public class SocketStreamSource implements StreamSource {
	private final Socket socket;
	private final SocketStream stream;

	private volatile boolean opened;

	public SocketStreamSource(Socket socket) {
		this.socket = socket;
		this.stream = new SocketStream();
	}

	@Override
	public Stream open() throws IOException {
		if (!opened) {
			opened = true;

			stream.inputStream = socket.getInputStream();
			stream.outputStream = socket.getOutputStream();

			if (stream.inputStream == null || stream.outputStream == null) {
				throw new IOException("stream is already closed");
			}

			socket.setTcpNoDelay(true);

			return stream;
		}
		throw new IOException("stream has already been opened.");
	}

	private class SocketStream implements Stream {
		private volatile InputStream inputStream;
		private volatile OutputStream outputStream;

		@Override
		public void close() throws IOException {
			socket.close();
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return inputStream;
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			return outputStream;
		}
	}
}

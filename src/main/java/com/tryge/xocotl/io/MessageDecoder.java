package com.tryge.xocotl.io;

import java.nio.ByteBuffer;

/**
 * A class a protocol implementation will have to implement in order to use any
 * utility functions provided by this package.
 *
 * @author michael.zehender@me.com
 */
public interface MessageDecoder {
	/**
	 * Decodes a (possibly not fully received) byte buffer and returns a message.
	 *
	 * @param buffer of the content received (so far)
	 * @return the next message or null if the message hasn't been fully
	 *         received
	 * @throws IllegalArgumentException if the content of the buffer is invalid
	 */
	Message decode(ByteBuffer buffer);
}

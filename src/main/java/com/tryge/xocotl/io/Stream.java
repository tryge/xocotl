package com.tryge.xocotl.io;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author michael.zehender@me.com
 */
public interface Stream extends Closeable {
	/**
	 * Closes the stream.
	 *
	 * @throws java.io.IOException
	 */
	void close() throws IOException;

	/**
	 * @return the input stream
	 */
	InputStream getInputStream() throws IOException;

	/**
	 * @return the output stream
	 */
	OutputStream getOutputStream() throws IOException;
}

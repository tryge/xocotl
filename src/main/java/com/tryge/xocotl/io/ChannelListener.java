package com.tryge.xocotl.io;

/**
 * Note that callbacks (except for onClose) are invoked from the reader thread
 * i.e. you'll have to pass the events to a worker thread in order to keep
 * receiving messages while processing the events.
 *
 * @author michael.zehender@me.com
 */
public interface ChannelListener {
	/**
	 * Called just before the channel is opened. It is possible to send messages,
	 * however they will be queued until the channel is actually opened.
	 *
	 * @param channel that is about to open
	 */
	void onOpen(DuplexChannel channel);

	/**
	 * Called when the channel is closed, either from the remote end or
	 * locally. Note that it is not possible to send messages anymore.
	 */
	void onClose(DuplexChannel channel);
}

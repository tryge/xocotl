package com.tryge.xocotl.io;

/**
 * @author michael.zehender@me.com
 */
public interface MessageListener {
	/**
	 * Notifies the listener that a message has been received. The listener
	 * must decide what to do with it and is also responsible for sending
	 * the response.
	 *
	 * @param source of the message
	 * @param msg that has been received
	 */
	void onMessage(DuplexChannel source, Message msg);
}

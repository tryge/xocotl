package com.tryge.xocotl.io;

/**
 * @author michael.zehender@me.com
 */
interface Responder {
	/**
	 * Cancels all the remaining messages from the specified context.
	 *
	 * @param context that has been destroyed
	 */
	void destroyed(Object context);

	/**
	 * Register the specified message to receive a response. Is used
	 * internally by channels.
	 *
	 * @param context of the message
	 * @param message to register
	 * @return a responder message that wraps the message and maintains a
	 *         future for waiting for the response
	 */
	ResponderMessage register(Object context, Message message);

	/**
	 * Received a response, the responder will find the corresponding
	 * message and mark it with response received. Any threads blocking
	 * on the corresponding future will be unblocked.
	 *
	 * @param context of the message
	 * @param response message
	 */
	void responseReceived(Object context, Message response);
}

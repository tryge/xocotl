package com.tryge.xocotl.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author michael.zehender@me.com
 */
class PoisonMessage implements Message {
	@Override
	public String getId() {
		return null;
	}

	@Override
	public String isResponseTo() {
		return null;
	}

	@Override
	public boolean isResponse() {
		return false;
	}

	@Override
	public void writeTo(OutputStream outputStream) throws IOException {
		throw new EOFException();
	}
}

package com.tryge.xocotl.io;

import com.tryge.xocotl.util.internal.FutureImpl;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author michael.zehender@me.com
 */
class ResponderMessage extends AbstractMessageDelegate {
	private final FutureImpl<Message> future;

	public ResponderMessage(Message message, Runnable doneAction) {
		super(message);
		future = new FutureImpl<Message>(doneAction);
	}

	FutureImpl<Message> future() {
		return future;
	}

	@Override
	public void writeTo(OutputStream outputStream) throws IOException {
		try {
			synchronized (future) {
				if (!future().isCancelled()) {
					future.running();
					message.writeTo(outputStream);
				}
			}
		} catch(Throwable th) {
			future.failed(th);
		}
	}
}

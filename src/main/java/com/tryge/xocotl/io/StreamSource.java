package com.tryge.xocotl.io;

import java.io.IOException;

/**
 * @author michael.zehender@me.com
 */
public interface StreamSource {
	/**
	 * Opens the stream.
	 *
	 * @throws IOException
	 */
	Stream open() throws IOException;
}

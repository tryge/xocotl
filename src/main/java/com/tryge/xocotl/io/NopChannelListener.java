package com.tryge.xocotl.io;

/**
 * @author michael.zehender@me.com
 */
public class NopChannelListener implements ChannelListener, MessageListener {
	@Override
	public void onOpen(DuplexChannel channel) {
	}

	@Override
	public void onClose(DuplexChannel channel) {
	}

	@Override
	public void onMessage(DuplexChannel source, Message msg) {
	}
}

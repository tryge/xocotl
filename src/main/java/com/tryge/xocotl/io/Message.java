package com.tryge.xocotl.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Represents a message.
 *
 * @author michael.zehender@me.com
 */
public interface Message {
	/**
	 * @return the (unique) id of this message - can be null if you
	 *         don't use <code>DuplexChannel#send()</code>
	 */
	String getId();

	/**
	 * @return the id of the message this message is a response to,
	 *         if this is a response, null otherwise
	 */
	String isResponseTo();

	/**
	 * @return whether this is a response message
	 */
	boolean isResponse();

	/**
	 * Write this message to the specified output stream.
	 *
	 * @param outputStream to write to
	 */
	void writeTo(OutputStream outputStream) throws IOException;
}

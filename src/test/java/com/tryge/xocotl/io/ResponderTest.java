package com.tryge.xocotl.io;

import com.tryge.xocotl.util.internal.FutureImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.EOFException;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class ResponderTest {
	private Message message;
	private Message response;

	@Before
	public void setUp() {
		message = mock(Message.class);
		response = mock(Message.class);
	}

	@Test
	public void testRegisterAndReceive() throws ExecutionException, InterruptedException {
		when(message.getId()).thenReturn("test");
		when(response.isResponseTo()).thenReturn("test");

		ResponderImpl responder = new ResponderImpl();
		ResponderMessage responderMessage = responder.register(this, message);
		FutureImpl<Message> future = responderMessage.future();

		responder.responseReceived(this, response);

		assertTrue(future.isDone());
		assertSame(response, future.get());
	}

	@Test
	public void testRegisterAndCancel() throws ExecutionException, InterruptedException {
		when(message.getId()).thenReturn("test");
		when(response.isResponseTo()).thenReturn("test");

		ResponderImpl responder = new ResponderImpl();
		ResponderMessage responderMessage = responder.register(this, message);
		FutureImpl<Message> future = responderMessage.future();
		future.cancel(true);

		// if the response is received after the cancellation
		responder.responseReceived(this, response);

		assertTrue(future.isDone());
		assertNull(future.get());
	}

	@Test
	public void testRegisterAndDestroy() throws ExecutionException, InterruptedException {
		when(message.getId()).thenReturn("test");
		when(response.isResponseTo()).thenReturn("test");

		ResponderImpl responder = new ResponderImpl();
		ResponderMessage responderMessage = responder.register(this, message);
		FutureImpl<Message> future = responderMessage.future();

		responder.destroyed(this);

		assertTrue(future.isDone());
		try {
			future.get();
			fail();
		} catch(ExecutionException e) {
			assertTrue(e.getCause() instanceof EOFException);
		}
	}

	@Test
	public void testRegisterAndCleanup() throws InterruptedException {
		when(message.getId()).thenReturn("test");
		when(response.isResponseTo()).thenReturn("test");

		ResponderImpl responder = new ResponderImpl();
		responder.register(this, message);
		assertEquals(1, responder.waitingMessages());

		Thread.sleep(1);

		responder.cleanup(System.currentTimeMillis());

		assertEquals(0, responder.waitingMessages());
	}
}

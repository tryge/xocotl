package com.tryge.xocotl.util;

import com.tryge.xocotl.io.Message;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class BinaryContainerTest {
	@Test
	public void testBinaryRoundtrip() throws Exception {
		byte[] data = randomData();

		byte[] encodedMessage = writeContainerMessage(data);

		BinaryContainerMessage decodedMessage = readContainerMessage(encodedMessage);

		assertArrayEquals(data, decodedMessage.getContent());
	}

	private BinaryContainerMessage readContainerMessage(byte[] encodedMessage) {
		BinaryContainerDecoder decoder = new BinaryContainerDecoder();
		Message decodedMessage = decoder.decode(ByteBuffer.wrap(encodedMessage));

		assertTrue(decodedMessage instanceof BinaryContainerMessage);

		return (BinaryContainerMessage)decodedMessage;
	}

	private byte[] writeContainerMessage(byte[] data) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		BinaryContainerMessage message = new BinaryContainerMessage(data);
		message.writeTo(out);
		return out.toByteArray();
	}

	private byte[] randomData() {
		byte[] data = new byte[312];

		new Random().nextBytes(data);
		return data;
	}
}

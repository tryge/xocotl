package com.tryge.xocotl.example;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws InterruptedException, IOException {
		ExampleServer server = new ExampleServer();
		server.open();
		server.start();

		ExampleClient client = new ExampleClient();

		Thread clientThread = new Thread(client);
		clientThread.start();

		// threads shutdown after server closes connection to client
	}
}

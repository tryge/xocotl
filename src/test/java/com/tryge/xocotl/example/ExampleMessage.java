package com.tryge.xocotl.example;

import com.tryge.xocotl.io.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class ExampleMessage implements Message {
	private final String id;
	private final String content;

	public ExampleMessage(String id, String content) {
		this.id = id;
		this.content = content;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String isResponseTo() {
		return null;
	}

	@Override
	public boolean isResponse() {
		return false;
	}

	@Override
	public void writeTo(OutputStream outputStream) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(outputStream);
		out.writeUTF(id);
		out.writeUTF(content);
		out.flush();
	}

	@Override
	public String toString() {
		return "id: " + id + " content: " + content;
	}
}

package com.tryge.xocotl.example;

import com.tryge.xocotl.io.DuplexChannel;
import com.tryge.xocotl.util.NopDecoder;
import com.tryge.xocotl.io.SocketChannels;

import java.net.Socket;

public class ExampleClient implements Runnable {
	@Override
	public void run() {
		try {
			Socket socket = new Socket("localhost", ExampleServer.PORT);
			DuplexChannel channel = SocketChannels.getFactory().newSocketChannel(socket, new NopDecoder());
			channel.open();
			channel.sendAndForget(new ExampleMessage("1", "test"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.tryge.xocotl.example;

import com.tryge.xocotl.io.*;
import com.tryge.xocotl.util.BoundedServer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ExampleServer {
	public static final int PORT = 6000;

	private volatile ServerSocket serverSocket;
	private volatile BoundedServer server;

	public void open() throws IOException {
		serverSocket = new ServerSocket(PORT);
	}

	public void start() {
		try {
			server = new BoundedServer.Builder()
				.setChannelFactory(SocketChannels.getFactory())
				.setMessageDecoder(new ExampleDecoder())
				.setMessageListener(new ExampleListener())
				.build();
			server.start(serverSocket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class ExampleDecoder implements MessageDecoder {
		@Override
		public Message decode(ByteBuffer buffer) {
			if (!buffer.hasRemaining()) {
				return null;
			}

			byte[] bytes = new byte[buffer.remaining()];
			buffer.get(bytes);

			System.out.println(Arrays.toString(bytes));

			try {
				ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes);
				ObjectInputStream objIn = new ObjectInputStream(bytesIn);

				String id = objIn.readUTF();
				String content = objIn.readUTF();

				return new ExampleMessage(id, content);
			} catch (Exception e) {
				return null;
			}
		}
	}

	class ExampleListener implements MessageListener {
		@Override
		public void onMessage(DuplexChannel channel, Message msg) {
			System.out.println("received: " + msg.toString());

			try {
				server.stop();
			} catch (Exception ignored) {
			}
		}
	}
}
